GL_EXT_demote_to_helper_invocation on radeonsi
EGL_MESA_platform_xcb
driconf: remove glx_disable_oml_sync_control, glx_disable_sgi_video_sync, and glx_disable_ext_buffer_age
Removed support for loading DRI drivers older than Mesa 8.0, including all DRI1 support
